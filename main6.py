#!/usr/bin/env python3

__author__ = 'Kacper Nyk'
__email__ = 'szkolatechi@op.pl'
"""Exercise from 71 page"""


def check(name: str) -> str:
    my_name: str = "Kacper"
    if name != my_name:
        print("Hello {}.".format(name))
    else:
        print("Welcome back {}.".format(my_name))


give_name: str = input("Type your name: ")
check(give_name)


class Rectangle:

    def __init__(self, length: int, width: int, name: str):
        self.length: int = length
        self.width: int = width
        self.name: str = name

    def __eq__(self, other):
        print("Comparision type: {} == {}".format(self.name, other.name))
        return self.length == other.length and (self.width == other.width)

    def __lt__(self, other):
        print("Comparision type: {} < {}".format(self.name, other.name))
        return self.length * self.width < other.length * other.width

    def __gt__(self, other):
        print("Comparision type: {} > {}".format(self.name, other.name))
        return self.length * self.width > other.length * other.width

    def __le__(self, other):
        print("Comparision type: {} <= {}".format(self.name, other.name))
        return self.length * self.width <= other.length * other.width

    def __ge__(self, other):
        print("Comparision type: {} >= {}".format(self.name, other.name))
        return self.length * self.width >= other.length * other.width

    def __ne__(self, other):
        print("Comparision type: {} != {}".format(self.name, other.name))
        return self.length * self.width != other.length * other.width


a = Rectangle(4, 20, "A")
b = Rectangle(13, 37, "B")

print("1. A == B = {}".format(a == b))
print("2. A < B = {}".format(a < b))
print("3. A > B = {}".format(a > b))
print("4. A <= B = {}".format(a <= b))
print("5. A >= B = {}".format(a >= b))
print("6. A != B = {}".format(a != b))

print()


class Cuboid:

    def __init__(self, length: int, width: int, height: int, name: str):

        self.length: int = length
        self.width: int = width
        self.height: int = height
        self.name: str = name

    def __eq__(self, other):
        print("Comparision type: {} == {}".format(self.name, other.name))
        return self.length == other.length and (self.width == other.width)

    def __lt__(self, other):
        print("Comparision type: {} < {}".format(self.name, other.name))
        return self.length * self.width * self.height < other.length * other.width * other.height

    def __gt__(self, other):
        print("Comparision type: {} > {}".format(self.name, other.name))
        return self.length * self.width * self.height > other.length * other.width * other.height

    def __le__(self, other):
        print("Comparision type: {} <= {}".format(self.name, other.name))
        return self.length * self.width * self.height <= other.length * other.width * other.height

    def __ge__(self, other):
        print("Comparision type: {} >= {}".format(self.name, other.name))
        return self.length * self.width * self.height >= other.length * other.width * other.height

    def __ne__(self, other):
        print("Comparision type: {} != {}".format(self.name, other.name))
        return self.length * self.width * self.height != other.length * other.width * other.height


A = Cuboid(4, 2, 3, "A")
B = Cuboid(6, 6, 6, "B")

print("A == B = {}".format(A == B))
print("A < B = {}".format(A < B))
print("A > B = {}".format(A > B))
print("A <= B = {}".format(A <= B))
print("A >= B = {}".format(A >= B))
print("A != B = {}".format(A != B))
