#!/usr/bin/env python3

__author__ = 'Kacper Nyk'
__email__ = 'szkolatechi@op.pl'

""" Exercise from 32 page"""


class Tree: pass


tree = Tree()

print(tree)
print(isinstance(tree, Tree))
print(isinstance(tree, object))


class AppleTree(Tree): pass


apple = AppleTree()

print(apple)
print(isinstance(apple, AppleTree))
print(isinstance(apple, Tree))
print(isinstance(apple, object))


class CherryTree(Tree): pass


cherry = CherryTree()

print(cherry)
print(isinstance(cherry, CherryTree))
print(isinstance(cherry, Tree))
print(isinstance(cherry, object))

print(isinstance(apple, CherryTree))
print(isinstance(cherry, AppleTree))
