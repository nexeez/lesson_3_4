#!/usr/bin/env python3

__author__ = 'Kacper Nyk'
__email__ = 'szkolatechi@op.pl'
"""Exercise from 56 page"""

name: str = input("Type your name: ")
surname: str = input("Type ur surname: ")
age: int = input("Type your age: ")

print(name, surname, age)


def number() -> None:
    numb = input("Type number: ")
    print(type(numb))
    numb = float(numb)
    print(isinstance(numb, float))
    print(type(numb))


number()


class Flower:
    def __init__(self, name: str, type: str, color: str):
        self.name = name
        self.type = type
        self.color = color

    def info(self):
        print("name: {} , type: {} , color: {}.".format(
            self.name, self.color, self.color
        ))


flow_name: str = input("Flower name: ")
flow_type: str = input("Flower type: ")
flow_color: str = input("Flower color: ")

flower = Flower(flow_name, flow_type, flow_color)
flower.info()
