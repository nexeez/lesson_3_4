#!/usr/bin/env python3

__author__ = 'Kacper Nyk'
__email__ = 'szkolatechi@op.pl'

""" Exercise from 41 page"""


def name() -> str:
    print("Kacper")


name()


def plus() -> int:
    print(200 + 200 + 20)


plus()


def greatings() -> str:

    return "Hello world"


print(greatings())


def minus(a: int, b: int) -> int:
    return a - b


print(minus(500, 80))


class Tree: pass


def new_tree() -> Tree:
    return Tree()


vegetation = new_tree()
print(type(vegetation))
