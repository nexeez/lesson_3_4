#!/usr/bin/env python3

__author__ = 'Kacper Nyk'
__email__ = 'szkolatechi@op.pl'
"""Exercise from 50 page"""

class Human:
    def __init__(self, name, surname, age):
        self.name = name
        self.surname = surname
        self.age = age

    def __repr__(self) -> str:
        return f"{self.name} {self.surname} {self.age}"


class Teacher(Human):
    def __init__(self, profession, *args):
        super(Teacher, self).__init__(*args)
        self.profession = profession

    def __repr__(self, *args):
        rep = super(Teacher, self).__repr__()
        return f"{rep} {self.profession}"


person = Teacher("Paweł", "Susi", "Math", "28")
teacher = Human("Mike", "Wazowski", 31)
print(isinstance(person, Teacher))
print(person)
print(teacher)
