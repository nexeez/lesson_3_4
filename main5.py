#!/usr/bin/env python3

__author__ = 'Kacper Nyk'
__email__ = 'szkolatechi@op.pl'
"""Exercise from 82 page"""


list_1 = [
    [1], [2], [3]
]

for number_1 in list_1:
    print(number_1)


list_2 = []

for number_2 in range(100, 115):
    list_2.append(number_2)

print(list_2)


list_3 = [5.12, 5.15, 7.73, 1.52, 7.12]

for number_3 in list_3:
    print(number_3)
